from flask import Flask, render_template
from sql import *

app = Flask(__name__)

@app.route('/')
def index():
	return render_template('base_layout.html')

@app.route('/about')
def home():
	return render_template('about.html')

@app.route('/members')
def members():
	
	data = s.query(Members).all()
	
	return render_template('members.html', data=data)

if __name__ == "__main__":
	app.run()
