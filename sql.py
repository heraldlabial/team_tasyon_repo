from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, VARCHAR, Text
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

class Members(Base):
    __tablename__ = 'members_finally'

    id = Column(Integer, primary_key=True)
    firstname = Column(VARCHAR(255), nullable=False)
    lastname = Column(VARCHAR(255), nullable=False)
    description = Column(Text(255), nullable=False)

    def __repr__(self):
		return "{} {} -{}".format(self.firstname, self.lastname, self.description)


engine = create_engine('mysql://heraldlabial:!heraldlabial@192.168.11.248/heraldlabial')


# CREATING A SESSION
Session = sessionmaker(bind=engine)

s = Session()

